#!/bin/bash

case "$1" in 
start)
   /app/go/bin/app > /tmp/imgmin.log &
   echo $!>/var/run/imgmin.pid
   ;;
stop)
   pkill app
	;;
restart)
   $0 stop
   $0 start
   ;;
status)
   if [ -e /var/run/imgmin.pid ]; then
      echo hit.sh is running, pid=`cat /var/run/imgmin.pid`
   else
      echo hit.sh is NOT running
      exit 1
   fi
   ;;
*)
   echo "Usage: $0 {start|stop|status|restart}"
esac

exit 0 
