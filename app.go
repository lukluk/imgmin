package main

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
	"gopkg.in/h2non/bimg.v1"

	"./templates"
)

const HOME = "http://demo.imgmin.co"

var MEDIA = os.Getenv("MEDIA")

const DEFAULT_QUALITY_MASTER = "83"

type Handler interface {
	ServeHTTP(w http.ResponseWriter, r *http.Request, next http.HandlerFunc)
}
type jsonobject struct {
	Title        string
	originalurl  string
	originalsize string
	minurl       string
	minsize      string
}
type ConfigSource struct {
	SourceUrl string
}

type JSONResponse struct {
	OriginalSize string
	MinSize      string
	OriginalUrl  string
	MinUrl       string
}

func Stat(subdomain string, path string) {
	t := time.Now()
	email, _ := client.Get(subdomain + "_owner").Result()
	numreq, _ := client.HGet(subdomain+"_request", t.Format("20060102")).Int64()
	numsize, _ := client.HGet(email, t.Format("20060102")).Float64()

	numreq = numreq + 1
	//fmt.Println("set" + subdomain + "_request " + strconv.FormatInt(numreq, 10))
	ee := client.HSet(subdomain+"_request", t.Format("20060102"), strconv.FormatInt(numreq, 10))
	fmt.Println(ee)
	fi, e := os.Stat(path)
	fmt.Println(e)
	if e == nil {
		fs := numsize + (float64(fi.Size()) / 1024)
		//fmt.Println("set" + subdomain + "_size " + strconv.FormatFloat(fs, 'f', 6, 64))
		er := client.HSet(email, t.Format("20060102"), strconv.FormatFloat(fs, 'f', 6, 64))
		fmt.Println(er)
	}
}
func check(err error) {
	if err != nil {
		fmt.Println("Error : %s", err.Error())
		os.Exit(1)
	}
}

func copy(src string, target string) {
	srcFile, err := os.Open(src)
	check(err)
	defer srcFile.Close()

	destFile, err := os.Create(target) // creates if file doesn't exist
	check(err)
	defer destFile.Close()

	_, err = io.Copy(destFile, srcFile) // check first var for number of bytes copied
	check(err)

	err = destFile.Sync()
	check(err)
}

func JPEGoptim(max string, target string) []byte {
	fmt.Println("SUC " + target)
	if strings.HasSuffix(target, ".png") {
		fmt.Println("pngquant " + "--force --output=" + target + " --quality=" + max + " " + target)
		err := exec.Command("pngquant", "--force", "--output="+target, "--quality="+max, target).Run()
		if err != nil {
			fmt.Println(err)
		}
		return nil
	} else {
		out, err := exec.Command("jpegoptim", "--max="+max, target).Output()
		if err != nil {
			fmt.Println(err)
		}
		return out
	}
}
func downloadFile(filepath string, url string) (err error) {

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}
func go_404(w http.ResponseWriter) {
	fmt.Fprintln(w, "404")
}

func contenttype(r *http.Request) (contentType string) {
	var path string
	path = r.URL.Path
	if strings.HasSuffix(path, ".css") {
		contentType = "text/css"
	} else if strings.HasSuffix(path, ".html") {
		contentType = "text/html"
	} else if strings.HasSuffix(path, ".js") {
		contentType = "application/javascript"
	} else if strings.HasSuffix(path, ".png") {
		contentType = "image/png"
	} else if strings.HasSuffix(path, ".svg") {
		contentType = "image/svg+xml"
	} else {
		contentType = "text/html"
	}

	return
}
func rotate(masterpath string, targetpath string, deg bimg.Angle) {
	buffer, err := bimg.Read(masterpath)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}

	newImage, err := bimg.NewImage(buffer).Rotate(deg)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}

	bimg.Write(targetpath, newImage)
}
func getImageDimension(imagePath string) (int, int) {
	file, err := os.Open(imagePath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
	}

	image, _, err := image.DecodeConfig(file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %v\n", imagePath, err)
	}
	return image.Width, image.Height
}
func resize(masterpath string, targetpath string, w int, h int, crop int) {
	buffer, err := bimg.Read(masterpath)
	fmt.Println("resize ->" + masterpath)
	if err != nil {
		fmt.Println("FUCKING ERROR")
		fmt.Fprintln(os.Stderr, err)
	}
	fmt.Println(crop)
	sizeWidth, sizeHeight := getImageDimension(masterpath)
	aspect := sizeWidth / sizeHeight
	fmt.Println(sizeWidth)
	fmt.Println(sizeHeight)
	if crop == 8 {
		fmt.Println("resize force ")
		newImage, err := bimg.NewImage(buffer).ForceResize(w, h)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		bimg.Write(targetpath, newImage)
	} else if crop == 9 {
		fmt.Println("resize clip ")
		newImage, err := bimg.NewImage(buffer).Resize(w, h)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		bimg.Write(targetpath, newImage)
	} else if crop < 0 {
		fmt.Println("resize original ")
		if sizeWidth > sizeHeight {
			if h >= w && w != 0 {
				h = 0
			}
		} else if sizeWidth < sizeHeight {
			if h <= w && h != 0 {
				w = 0
			}
		} else {
			h = 0
		}

		newImage, err := bimg.NewImage(buffer).Resize(w, h)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		bimg.Write(targetpath, newImage)
	} else {
		fmt.Println("croping ")
		wi := w
		hi := h
		if w > 0 && h <= 0 {
			h = int(w / aspect)
		} else if h > 0 && w <= 0 {
			w = int(h * aspect)
		}
		if w > sizeWidth || h > sizeHeight {
			//@TODO: duplicated should removed
			if w > sizeWidth {
				wi = sizeWidth
			}
			if h > sizeHeight {
				hi = sizeHeight
			}
			fmt.Println("resize original ")
			if sizeWidth > sizeHeight {
				if hi >= wi && wi != 0 {
					hi = 0
				}
			} else if sizeWidth < sizeHeight {
				if hi <= wi && hi != 0 {
					wi = 0
				}
			} else {
				hi = 0
			}

			newImage, err := bimg.NewImage(buffer).Resize(wi, hi)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
			}
			bimg.Write(targetpath, newImage)
		} else {
			top := 1
			left := 1
			switch crop {
			case 0:
				left = int((sizeWidth - w) / 2)
				top = int((sizeHeight - h) / 2)
			case 1:
				left = int((sizeWidth - w) / 2)
				top = 1
			case 2:
				left = 1
				top = int((sizeHeight - h) / 2)
			case 3:
				left = int((sizeWidth - w) / 2)
				top = sizeHeight - h
			case 4:
				left = sizeWidth - w
				top = int((sizeHeight - h) / 2)
			case 5:
				left = 1
				top = 1
			}
			options := bimg.Options{
				AreaWidth:  w,
				AreaHeight: h,
				Left:       left,
				Top:        top,
			}

			newImage, err := bimg.NewImage(buffer).Process(options)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
			}
			bimg.Write(targetpath, newImage)
		}
	}

}
func home(w http.ResponseWriter, r *http.Request) {
	var buf bytes.Buffer
	vars := mux.Vars(r)
	rest := strings.Replace(vars["rest"], ".html", "", 1)
	originalurl := vars["https"] + "://" + vars["client"] + "/" + rest + "?" + r.URL.RawQuery

	modified := HOME + "/api/" + vars["https"] + "/" + vars["client"] + "/" + rest + "?" + r.URL.RawQuery

	templates.WriteCompare(&buf, "Demo for "+vars["client"], originalurl, imgsize(originalurl), modified, imgsize(modified))
	fmt.Fprintf(w, "%s", buf.Bytes())
}
func homeJSON(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	rest := strings.Replace(vars["rest"], ".json", "", 1)
	originalurl := vars["https"] + "://" + vars["client"] + "/" + rest + "?" + r.URL.RawQuery

	modified := HOME + "/api/" + vars["https"] + "/" + vars["client"] + "/" + rest + "?" + r.URL.RawQuery
	w.Header().Set("Content-Type", "application/json")
	jsonResponse := JSONResponse{
		OriginalSize: imgsize(originalurl),
		OriginalUrl:  originalurl,
		MinSize:      imgsize(modified),
		MinUrl:       modified,
	}
	json.NewEncoder(w).Encode(jsonResponse)
}
func imgsize(url string) (sizeo string) {
	// we are interested in getting the file or object name
	// so take the last item from the slice

	resp, err := http.Head(url)
	if err != nil {
		sizeo = "error"
		return
	}

	// Is our request ok?

	if resp.StatusCode != http.StatusOK {
		fmt.Println(resp.Status)
		sizeo = "error"
		return
		// exit if not ok
	}

	// the Header "Content-Length" will let us know
	// the total file size to download
	size, _ := strconv.Atoi(resp.Header.Get("Content-Length"))
	downloadSize := int(size) / 1024
	sizeo = strconv.Itoa(downloadSize) + "kb"
	return

}
func imageproc(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	subdomain := strings.Split(r.Host, ".")
	//subdomain[0] = "loket"
	fmt.Println(subdomain)
	var config ConfigSource
	val, err := client.Get(subdomain[0] + "_webfolder").Result()
	if err == nil {
		fmt.Println("redis on")
		// ival := []byte(val)
		// json.Unmarshal(ival, &config)
		// if err != nil {
		// 	fmt.Println("error:", err)
		// }
		config.SourceUrl = val
	} else if _, err := os.Stat("./source/" + subdomain[0] + ".json"); !os.IsNotExist(err) {
		raw, err := ioutil.ReadFile("./source/" + subdomain[0] + ".json")
		if err != nil {
			fmt.Println(err.Error())
		}

		var c ConfigSource
		json.Unmarshal(raw, &c)
		config.SourceUrl = c.SourceUrl
	}
	vars := mux.Vars(r)

	var originalurl string
	if config.SourceUrl != "" {
		originalurl = config.SourceUrl + "/" + vars["rest"]
	} else if strings.Contains(vars["rest"], "api") {
		rest := strings.Replace(vars["rest"], "api/", "", 1)
		paths := strings.Split(rest, "/")
		paths[0] = paths[0] + ":/"
		originalurl = strings.Join(paths, "/")
		fmt.Println("API>" + originalurl)
	} else {
		go_404(w)
		return
	}
	var ext string
	ext = "jpg"
	if strings.HasSuffix(originalurl, ".png") {
		ext = "png"
	} else if strings.HasSuffix(originalurl, ".JPG") {
		ext = "JPG"
	} else if strings.HasSuffix(originalurl, ".PNG") {
		ext = "PNG"
	}
	fmt.Println(originalurl + ext)
	masterpath := MEDIA + "/" + GetMD5Hash(originalurl) + "." + ext
	targetpath := MEDIA + "/" + GetMD5Hash(r.URL.String()) + "." + ext
	fmt.Println(masterpath + " " + targetpath)
	if _, err := os.Stat(targetpath); err == nil {
		fmt.Println("get iiii cache " + targetpath)
		go Stat(subdomain[0], targetpath)
		http.ServeFile(w, r, targetpath)
	} else {
		if _, err := os.Stat(masterpath); os.IsNotExist(err) {
			fmt.Println("download " + masterpath)
			err_download := downloadFile(masterpath, originalurl)
			if err_download != nil {
				fmt.Println("FAILED to DOWNLOAD")
				go_404(w)
			} else {
				JPEGoptim(DEFAULT_QUALITY_MASTER, masterpath)
			}
		}
		if _, err := os.Stat(masterpath); err == nil {

			if r.URL.RawQuery == "" {
				elapsed := time.Since(start)
				fmt.Println("Binomial took %s", elapsed)
				go Stat(subdomain[0], masterpath)
				http.ServeFile(w, r, masterpath)
				return
			}
			fmt.Println("get cache master " + masterpath)
			W, _ := strconv.Atoi(r.URL.Query().Get("w"))
			Q := r.URL.Query().Get("q")
			Deg, _ := strconv.Atoi(r.URL.Query().Get("deg"))
			Crop := r.URL.Query().Get("crop")
			Fit := r.URL.Query().Get("fit")
			H, _ := strconv.Atoi(r.URL.Query().Get("h"))
			processed := false
			var crop int

			switch Fit {
			case "crop":
				crop = 0
			case "clip":
				crop = 9
			case "foce":
				crop = 8
			default:
				crop = -1
			}
			switch Crop {
			case "edge":
				crop = 0
			case "top":
				crop = 1
			case "bottom":
				crop = 3
			case "left":
				crop = 2
			case "right":
				crop = 4
			case "rest":
				crop = 5
			default:
				crop = -1
			}

			if W > 0 || H > 0 {
				fmt.Println("resize " + masterpath + "crop = " + Crop)
				fmt.Println(W)
				fmt.Println(H)
				resize(masterpath, targetpath, W, H, crop)
				processed = true
			}
			if Deg > 0 {
				var deg bimg.Angle
				switch {
				case Deg > 0 && Deg <= 45:
					deg = bimg.D45
				case Deg > 45 && Deg <= 90:
					deg = bimg.D90
				case Deg > 90 && Deg <= 135:
					deg = bimg.D135
				case Deg > 135 && Deg <= 180:
					deg = bimg.D180
				case Deg > 180 && Deg <= 235:
					deg = bimg.D235
				case Deg > 235 && Deg <= 270:
					deg = bimg.D270
				case Deg > 270 && Deg < 360:
					deg = bimg.D315
				case Deg >= 360:
					deg = bimg.D0
				default:
					deg = bimg.D0
				}
				if processed == false {
					rotate(masterpath, targetpath, deg)
				} else {
					rotate(targetpath, targetpath, deg)
				}
				processed = true
			}

			if Q != "" {
				fmt.Println("Qulity to " + Q)
				if processed == false {
					copy(masterpath, targetpath)
				}
				JPEGoptim(Q, targetpath)
				processed = true
			}
			if processed == false {
				go Stat(subdomain[0], masterpath)
				http.ServeFile(w, r, masterpath)
			} else {
				go Stat(subdomain[0], targetpath)
				http.ServeFile(w, r, targetpath)
			}
		}
	}

}

var client *redis.Client

//var Configs ConfigSource[]
func main() {
	fmt.Println("versi 1.0.4")
	client = redis.NewClient(&redis.Options{
		Addr:     "10.130.27.112:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	r := mux.NewRouter()
	r.PathPrefix("/assets").Handler(http.StripPrefix("/assets", http.FileServer(http.Dir("./static/"))))
	r.PathPrefix("/media").Handler(http.StripPrefix("/media", http.FileServer(http.Dir("/media/"))))
	r.PathPrefix("/tmp").Handler(http.StripPrefix("/app/tmp", http.FileServer(http.Dir("/app/tmp/"))))
	r.HandleFunc("/{https}/{client}/{rest:.*.json}", homeJSON)
	r.HandleFunc("/{https}/{client}/{rest:.*.html}", home)
	r.HandleFunc("/{rest:.*.jpg}", imageproc)
	r.HandleFunc("/{rest:.*.JPG}", imageproc)
	r.HandleFunc("/{rest:.*.jpeg}", imageproc)
	r.HandleFunc("/{rest:.*.png}", imageproc)
	r.HandleFunc("/{rest:.*.PNG}", imageproc)
	http.ListenAndServe(":9001", r)

}
