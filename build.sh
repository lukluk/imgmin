#!/bin/bash
cd /app
export GOPATH=/app
export GOBIN=/app/bin
go get
go get -u github.com/valyala/quicktemplate
go get -u github.com/valyala/quicktemplate/qtc
go build
