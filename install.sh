apt-get install gtk-doc
sudo apt-get install gtk-doc-tools
apt-get install swig
apt-get install glib2.0-dev
apt-get install gobject-introspection
apt-get install libgirepository1.0-dev
sudo apt-get install build-essential chrpath libssl-dev libxft-dev -y
sudo apt-get install libfreetype6 libfreetype6-dev -y
sudo apt-get install libfontconfig1 libfontconfig1-dev -y
sudo apt-get install libfontconfig1 libfontconfig1-dev -y
sudo apt-get install libvips
sudo apt-get install libvips-tools
apt-get install libvips-dev
apt-get install jpegoptim
apt-get install pngquant
cd /root
wget https://github.com/jcupitt/libvips/archive/v8.5.8.zip
apt-get install unzip
unzip v8.5.8.zip
./autogen.sh
make
make install
cd /app
go get
go build
